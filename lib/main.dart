import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_silver_bar/core/charts/custom_rounded_bars.dart';
import 'package:flutter_silver_bar/core/charts/grouped_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/grouped_bar_single_target_line_chart.dart';
import 'package:flutter_silver_bar/core/charts/grouped_bar_target_line_chart.dart';
import 'package:flutter_silver_bar/core/charts/grouped_fill_color_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/grouped_stacked_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/grouped_stacked_weight_pattern_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/horizontal_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/horizontal_bar_label_chart.dart';
import 'package:flutter_silver_bar/core/charts/horizontal_bar_label_custom_chart.dart';
import 'package:flutter_silver_bar/core/charts/horizontal_pattern_forward_hatch_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/pattern_forward_hatch_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/simple_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/spark_bar.dart';
import 'package:flutter_silver_bar/core/charts/stacked_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/stacked_bar_target_line_chart.dart';
import 'package:flutter_silver_bar/core/charts/stacked_fill_color_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/stacked_horizontal_bar_chart.dart';
import 'package:flutter_silver_bar/core/charts/vertical_bar_label_chart.dart';
import 'package:flutter_silver_bar/ui/values/styles.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarBrightness: Brightness.dark,
  ));

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      theme: themeApp,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_controller.offset > _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      /*setState(() {
        clr = Colors.red;
      });*/
      print("Fechando" + _controller.offset.toString());
    }

    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      /*setState(() {
        clr = Colors.lightGreen;
      });*/
      print("Aberto");
    }
  }

  Widget listItem(int index, String title, Orientation orientation) => Card(
      elevation: 1,
      child: Container(
        height: 100.0,
        child: (index == 0)
            ? Padding(
                padding: EdgeInsets.all(4),
                child: SimpleBarChart.withRandomData())
            : (index == 1)
                ? Padding(
                    padding: EdgeInsets.all(4),
                    child: StackedBarChart.withRandomData())
                : (index == 2)
                    ? Padding(
                        padding: EdgeInsets.all(4),
                        child: GroupedBarChart.withRandomData())
                    : (index == 3)
                        ? Padding(
                            padding: EdgeInsets.all(4),
                            child: GroupedStackedBarChart.withRandomData())
                        : (index == 4)
                            ? Padding(
                                padding: EdgeInsets.all(4),
                                child:
                                    GroupedBarTargetLineChart.withRandomData())
                            : (index == 5)
                                ? Padding(
                                    padding: EdgeInsets.all(4),
                                    child: GroupedBarSingleTargetLineChart
                                        .withRandomData())
                                : (index == 6)
                                    ? Padding(
                                        padding: EdgeInsets.all(4),
                                        child: StackedBarTargetLineChart
                                            .withRandomData())
                                    : (index == 7)
                                        ? Padding(
                                            padding: EdgeInsets.all(4),
                                            child: HorizontalBarChart
                                                .withRandomData())
                                        : (index == 8)
                                            ? Padding(
                                                padding: EdgeInsets.all(4),
                                                child: StackedHorizontalBarChart
                                                    .withRandomData())
                                            : (index == 9)
                                                ? Padding(
                                                    padding: EdgeInsets.all(4),
                                                    child: HorizontalBarLabelChart.withRandomData(
                                                        orientation))
                                                : (index == 10)
                                                    ? Padding(
                                                        padding:
                                                            EdgeInsets.all(4),
                                                        child: HorizontalBarLabelCustomChart
                                                            .withRandomData(
                                                                orientation))
                                                    : (index == 11)
                                                        ? Padding(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    4),
                                                            child: VerticalBarLabelChart
                                                                .withRandomData())
                                                        : (index == 12)
                                                            ? Padding(
                                                                padding:
                                                                    EdgeInsets.all(
                                                                        8),
                                                                child: SparkBar
                                                                    .withRandomData())
                                                            : (index == 13)
                                                                ? Padding(
                                                                    padding:
                                                                        EdgeInsets.all(4),
                                                                    child: GroupedFillColorBarChart.withRandomData())
                                                                : (index == 14)
                                                                    ? Padding(padding: EdgeInsets.all(4), child: StackedFillColorBarChart.withRandomData())
                                                                    : (index == 15)
                                                                        ? Padding(padding: EdgeInsets.all(4), child: PatternForwardHatchBarChart.withRandomData())
                                                                        : (index == 16)
                                                                            ? Padding(padding: EdgeInsets.all(4), child: HorizontalPatternForwardHatchBarChart.withRandomData())
                                                                            : (index == 17)
                                                                                ? Padding(padding: EdgeInsets.all(4), child: GroupedStackedWeightPatternBarChart.withRandomData())
                                                                                : (index == 18)
                                                                                    ? Padding(padding: EdgeInsets.all(4), child: CustomRoundedBars.withRandomData())
                                                                                    : Center(
                                                                                        child: Text(
                                                                                          "$title",
                                                                                          textAlign: TextAlign.center,
                                                                                          style: Theme.of(context).textTheme.display1,
                                                                                        ),
                                                                                      ),
      ));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: OrientationBuilder(builder: (context, orientation) {
        return CustomScrollView(
          controller: _controller,
          slivers: <Widget>[
            SliverAppBar(
              centerTitle: true,
              title: Text(
                "Flutter Charts",
              ),
              floating: false,
              pinned: true,
              snap: false,
              leading: Icon(Icons.menu),
              backgroundColor: Colors.red[500],
              expandedHeight: 150.0,
              brightness: Brightness.light,
              flexibleSpace: const FlexibleSpaceBar(
                  background: IconButton(
                padding: EdgeInsets.only(top: 70),
                icon: Icon(
                  Icons.account_balance,
                  size: 80.0,
                  color: Colors.white,
                ),
              )),
            ),
            SliverPadding(
              padding: EdgeInsets.all(8),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  ///no.of items in the horizontal axis
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                  crossAxisCount: 2,
                ),

                ///Lazy building of list
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    /// To convert this infinite list to a list with "n" no of items,
                    /// uncomment the following line:
                    /// if (index > n) return null;
                    return listItem(
                        index, "Sliver Grid item:\n$index", orientation);
                  },

                  /// Set childCount to limit no.of items
                  childCount: 19,
                ),
              ),
            ),
          ],
        );
      }),
    );
  }
}
