import 'package:flutter/material.dart';

ThemeData themeApp = ThemeData(
  primarySwatch: Colors.red,
  primaryColor: Colors.red[500],
  accentColor: Colors.red[200],
  textTheme: TextTheme(
    display1: TextStyle(
      color: Colors.grey[600],
      fontSize: 14.0,
      fontWeight: FontWeight.bold,
    ),
  ),
);
